<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'clint');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#%;GBamSGzV2QH$MIXiyUZ&n5+-M1fSvuW@QUhA:xE/Spuw#wtfGU(vhhn<r5YZ^');
define('SECURE_AUTH_KEY',  '<yxBKu<Al3[#mSzJsvbua v+,D{a!+B%wbNkd83t=n:u-CMh({kCt1[brqHQo.qo');
define('LOGGED_IN_KEY',    '`@odUVZwn$,D$5pYg@g#yXi>LmJ[9JxJ.6ls%@zhnZM?MBP@?Ls){-2`U!2C|P;x');
define('NONCE_KEY',        '[[G405i^eZlY7J,a;k*:SPN`^kB7$r&w:#nGO%xE:*3?Nq?*G _/yWh/NibN1AX,');
define('AUTH_SALT',        'K=>X4%[rj4K1+t`eFX}|lgbp/k<[<S,kIv)4aE;fn.R1$hv[UqvvT5_-8rB|BU#~');
define('SECURE_AUTH_SALT', 'El@oA8G$SHN2@PYW<PLu*Ze%Q$QyQ,*mi%uR-Vdg(ey0<.?k(k{7^uTjysDMo)Cp');
define('LOGGED_IN_SALT',   '1$^T&Mgf^!U/FiJ&w]bpp[0Rv=^DW}1Mi<zTm,~E_V!,g>o;W>w=%e[XO,wUZ8Gy');
define('NONCE_SALT',       '5^Fr46`P/o=k2kht}+caXjs^l!~4%cxgLL552]#u07eoOg8^n]n7iJ?V<KzyuK~N');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'cl_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
