<?php get_header(); ?>

			<section id="home" class="wrap">
				
				<div >
					<?php

						$args = array (
							'page_id'=> '2',
						);

						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								echo '<h1>' . get_the_title() . '</h1>';
								echo '<p>' . get_the_content() . '</p>';
							}
						}
						wp_reset_postdata();
					?>
					<a href="#inscrever">Inscreva-se agora</a>
				</div>
			</section>

			<section id="o-que-e">
				<div class="wrap">
					<?php

						$args = array (
							'page_id'=> '17',
						);

						$the_query = new WP_Query( $args );
						if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
								$the_query->the_post();
								echo '<h1>' . get_the_title() . '</h1>';
								echo '<p>' . get_the_content() . '</p>';
							}
						}
						wp_reset_postdata();
					?>
				</div>
			</section>

			<section id="cidades">
				<div class="wrap">
					<h1>Cidades</h1>
					<div class="lista">
						<?php

							$args = array (
								'post_type'=> 'cidade',
								'posts_per_page' => '4'
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); ?>
									<div class="cidade">
										<?php the_post_thumbnail('thumb-cidade'); ?>
										<div>
											<h1><?php the_title() ?></h1>
											<h2><?php echo rwmb_meta( 'titulo', array()) ?></h2>
											<span><?php echo rwmb_meta( 'data', array()) ?></span>
											<a href="#cidade-<?php echo get_the_ID() ?>">Programação</a>
										</div>
									</div>
								<?php }
							}
							wp_reset_postdata();
						?>
					</div>
				</div>
			</section>


			<section id="palestrantes">
				<div class="wrap">
					<h1>Palestrantes</h1>
					<div class="lista">
						<?php

							$args = array (
								'post_type'=> 'palestrante',
								'posts_per_page' => '6'
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); ?>
									<div class="palestrante">
										<figure><?php the_post_thumbnail('thumb-palestrante'); ?></figure>
										<h1><?php the_title() ?></h1>
										<span><?php echo rwmb_meta( 'titulo', array()) ?></span>
									</div>
								<?php }
							}
							wp_reset_postdata();
						?>
					</div>
				</div>
			</section>


			<section id="o-que-esperar">
				<div class="wrap">
					<h1>O que esperar</h1>
					<div class="lista">
						<?php

							$args = array (
								'post_type'=> 'box',
								'posts_per_page' => '3'
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); ?>
									<div class="item-detalhe">
										<figure><?php the_post_thumbnail('thumb-palestrante'); ?></figure>
										<h1><?php the_title() ?></h1>
										<p><?php echo get_the_content() ?></p>
									</div>
								<?php }
							}
							wp_reset_postdata();
						?>
					</div>
				</div>
			</section>


			<section id="patrocinadores">
				<div class="wrap">
					<div class="patrocinador">
						<h1>Patrocínio Nacional</h1>
						<?php

							$args = array (
								'post_type'=> 'patrocinador',
								'meta_query'             => array(
								    array(
								        'key'       => 'tipo',
								        'value'     => 'patrocinio_nacional',
								    ),
								),
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); ?>
									<div><?php the_post_thumbnail('thumb-palestrante'); ?></div>
								<?php }
							}
							wp_reset_postdata();
						?>
					</div>
					<div class="patrocinador">
						<h1>Apoio</h1>
						<?php

							$args = array (
								'post_type'=> 'patrocinador',
								'meta_query'             => array(
								    array(
								        'key'       => 'tipo',
								        'value'     => 'apoio',
								    ),
								),
							);

							$the_query = new WP_Query( $args );
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { 
									$the_query->the_post(); ?>
									<div><?php the_post_thumbnail('thumb-palestrante'); ?></div>
								<?php }
							}
							wp_reset_postdata();
						?>
					</div>
				</div>
			</section>


			<section id="faq">
				<div class="wrap">
					<h1>Perguntas Frequentes</h1>
					<a href="#form-faq" class="botao-duvidas">Tire suas dúvidas</a>
					<div class="icones-sociais">
						<a href="#facebook"><i class="icon facebook"></i></a>
						<a href="#linkedn"><i class="icon linkedn"></i></a>
						<a href="#youtube"><i class="icon youtube"></i></a>
						<a href="#instagram"><i class="icon instagram"></i></a>
					</div>
				</div>
			</section>

<?php get_footer(); ?>
