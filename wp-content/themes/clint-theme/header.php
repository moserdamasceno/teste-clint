<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?php wp_title(''); ?></title>

		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<meta name="theme-color" content="#121212">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>
	</head>

	<body>

		<div id="container">

			<header id="header">

				<div id="inner-header" class="wrap">

					<div id="logo">
						<a href="<?php echo home_url(); ?>" rel="nofollow">
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-clint.png" alt="Logo Clint">
						</a>
					</div>

					<nav role="navigation">
						<?php wp_nav_menu(array(
								 'container' => false,                           // remove nav container
								 'container_class' => 'menu',                 // class of container (should you choose to use it)
								 'menu' => __( 'Menu Principal', 'menu-principal' ),  // nav name
								 'menu_class' => 'nav top-nav cf',               // adding custom nav class
								 'theme_location' => 'main-nav',                 // where it's located in the theme
								 'before' => '',                                 // before the menu
								   'after' => '',                                  // after the menu
								   'link_before' => '',                            // before each link
								   'link_after' => '',                             // after each link
								   'depth' => 0,                                   // limit the depth of the nav
								 'fallback_cb' => ''                             // fallback function (if there is one)
						)); ?>

					</nav>
					<a href="#menu" class="responsive-menu">
						☰ MENU
					</a>

				</div>




			</header>
